<?php

namespace Examinr\SecurityBundle\Controller;

use Examinr\SecurityBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        $user = $this->getUser();
        if ($user) {
            return $this->redirect($this->generateUrl('modules'));
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrSecurityBundle:User');

        $user = $repository->findOneByUsername('admin');
        if ($user == null && $repository->count() == 0) {
            $user = new User();
            $user->setUsername('admin');
            $user->setPassword('admin123');
            $user->setRole('ROLE_ADMIN');
            $user->setFullname('System Administrator');

            $em->persist($user);
            $em->flush();
        }

        $sesion = $request->getSession();
        $error = $request->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );
        return $this->render('ExaminrSecurityBundle:Security:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'errors' => $error
        ));
    }

    public function recoverPasswordAction (Request $request)
    {
//        $user = $this->getUser();
//        if ($user)
//            return $this->redirect($this->generateUrl('homepage'));
//
//        $em = $this->getDoctrine()->getManager();
//        $persona = $em->getRepository('ilmatohgestionCursosCoreBundle:DPersona')->findOneByEmail($request->get('email'));
//        if ($persona) {
//            $password = $this->RandomString();
//            $persona->getUsuario()->setContrasenna($password);
//            $em->persist($persona);
//            $em->flush();
//
//            $message = \Swift_Message::newInstance()
//                ->setSubject('Recuperación de contraseña')
//                ->setFrom('soporte@gesti.ilmatoh.com')
//                ->setTo($persona->getEmail())
//                ->setBody('Hola ' . $persona->getNombreCompleto() . ', su contraseña se ha cambiado satisfactoriamente por:' . $password);
//            $this->get('mailer')->send($message);
//        }
        return $this->redirect($this->generateUrl('usuario_login'));
    }

    function RandomString($length=10,$uc=TRUE,$n=TRUE,$sc=FALSE)
    {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1) $source .= '1234567890';
        if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                $rstr .= $source[$num-1];
            }

        }
        return $rstr;
    }
}