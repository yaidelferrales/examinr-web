<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class SurveyAnswerQuestionRepository extends EntityRepository
{
    public function findByAnswer($answer_id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT ea FROM ExaminrCoreBundle:SurveyAnswerQuestion ea INNER JOIN ea.surveyAnswer e WHERE e.id=" . $answer_id . "");
        $results = $query->getResult();

        return sizeof($results) > 0 ? $results : [];
    }
}