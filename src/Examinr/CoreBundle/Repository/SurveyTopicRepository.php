<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class SurveyTopicRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        $em = $this->getEntityManager();

        $start = is_numeric($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $lenght = is_numeric($request->query->get('length')) ? intval($request->query->get('length')) : 10;
//        if (!$start % $lenght != 0) {
//            $start = 0;
//        }

        $search = $request->query->get('search')['value'];

        $sortColumn = strtolower($request->query->get('order')[0]['column']);
        $sortColumn = $request->query->get('columns')[intval($sortColumn)]['name'];
        if ($sortColumn != 'name' && $sortColumn != 'questions' && $sortColumn != 'active') {
            $sortColumn = 'name';
        }

        $sortOrder = isset($request->query->get('order')[0]['dir']) ? $request->query->get('order')[0]['dir'] : 'asc';
        if ($sortOrder != 'asc' && $sortOrder != 'desc') {
            $sortOrder = 'asc';
        }

        $recordsFilteredCount = $em->createQuery("SELECT COUNT(e) FROM ExaminrCoreBundle:SurveyTopic e WHERE e.name LIKE " . "'%" . $search . "%' AND e.deleted = " . $deleted)->getSingleScalarResult();
        if ($start == $recordsFilteredCount) {
            $start -= $start == 0 ? 0 : $lenght;
        }
        $recordsFilteredItems = $em->createQuery("SELECT e FROM ExaminrCoreBundle:SurveyTopic e WHERE e.name LIKE " . "'%" . $search . "%' AND e.deleted = " . $deleted . " ORDER BY e." . $sortColumn . " " . $sortOrder)
            ->setFirstResult($start)
            ->setMaxResults($lenght)
            ->getResult();

//        //CONCAT(d.estudiante_id, '') LIKE '%3%'

        return array(
            "recordsFiltered" => $recordsFilteredCount,
            "data" => $recordsFilteredItems
        );
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrCoreBundle:SurveyTopic e WHERE e.deleted = " . $deleted);

        return $qb->getSingleScalarResult();
    }

    public function allTopics ()
    {
        $em = $this->getEntityManager();

        return $em->createQuery("SELECT partial e.{id,name} FROM ExaminrCoreBundle:SurveyTopic e WHERE e.active = true AND  e.deleted = false")->getResult();
    }
}