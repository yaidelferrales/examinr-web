<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class ExamAnswerRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        $em = $this->getEntityManager();

        $start = is_numeric($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $lenght = is_numeric($request->query->get('length')) ? intval($request->query->get('length')) : 10;

        $search = $request->query->get('search')['value'];

        $sortColumn = strtolower($request->query->get('order')[0]['column']);
        $sortColumn = $request->query->get('columns')[intval($sortColumn)]['name'];

        $sortOrder = isset($request->query->get('order')[0]['dir']) ? $request->query->get('order')[0]['dir'] : null;
        if ($sortOrder != null && $sortOrder != 'asc' && $sortOrder != 'desc') {
            $sortOrder = 'desc';
        }

        $exam = $request->query->get('columns')[0]['search']['value'];

        $recordsFilteredCount = $em->createQuery("SELECT COUNT(e) FROM ExaminrCoreBundle:ExamAnswer e INNER JOIN e.exam e1 INNER JOIN e.student e2 WHERE (e1.name LIKE " . "'%" . $search . "%' OR e2.dni LIKE " . "'%" . $search . "%') " . (strlen($exam) != 0 ? "AND e1.id = '" . $exam . "'" : ""))->getSingleScalarResult();
        if ($start >= $recordsFilteredCount) {
            $start = intval($recordsFilteredCount / $lenght) * $lenght;
        }

        $recordsFilteredItems = $em->createQuery("SELECT e FROM ExaminrCoreBundle:ExamAnswer e INNER JOIN e.exam e1 INNER JOIN e.student e2 WHERE (e1.name LIKE '%" . $search . "%' OR e2.dni LIKE '%" . $search . "%')" . (strlen($exam) != 0 ? " AND e1.id = '" . $exam . "'" : "") . " ORDER BY " . ($sortColumn == "exam" ? " e1.name " : ($sortColumn == "student" ? " e2.dni " : "e.id ")) . $sortOrder)
            ->setFirstResult($start)
            ->setMaxResults($lenght)
            ->getResult();

        return array(
            "recordsFiltered" => $recordsFilteredCount,
            "data" => $recordsFilteredItems
        );
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrCoreBundle:ExamAnswer e");

        return $qb->getSingleScalarResult();
    }

    public function findByStudentAndExam($student, $exam)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT ea FROM ExaminrCoreBundle:ExamAnswer ea INNER JOIN ea.exam e INNER JOIN ea.student s WHERE s.dni = '" . $student->getDni() . "' AND e.id = " . $exam->getId());
        $results = $query->getResult();

        return sizeof($results) > 0 ? $results[0] : null;
    }

    public function findByExam($exam_id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT ea FROM ExaminrCoreBundle:ExamAnswer ea INNER JOIN ea.exam e INNER JOIN ea.student s WHERE e.id=" . $exam_id . "");
        $results = $query->getResult();

        return sizeof($results) > 0 ? $results : [];
    }
}