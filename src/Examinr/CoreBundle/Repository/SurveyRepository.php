<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class SurveyRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        $em = $this->getEntityManager();

        $start = is_numeric($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $lenght = is_numeric($request->query->get('length')) ? intval($request->query->get('length')) : 10;

        $search = $request->query->get('search')['value'];

        $sortColumn = strtolower($request->query->get('order')[0]['column']);
        $sortColumn = $request->query->get('columns')[intval($sortColumn)]['name'];
        if ($sortColumn != 'teacher' && $sortColumn != 'paralelo' && $sortColumn != 'topic') {
            $sortColumn = 'teacher';
        }

        $sortOrder = isset($request->query->get('order')[0]['dir']) ? $request->query->get('order')[0]['dir'] : 'asc';
        if ($sortOrder != 'asc' && $sortOrder != 'desc') {
            $sortOrder = 'asc';
        }

        $topic = $request->query->get('columns')[0]['search']['value'];

        $recordsFilteredCount = $em->createQuery("SELECT COUNT(e) FROM ExaminrCoreBundle:Survey e INNER JOIN e.topic m WHERE (e.teacher LIKE " . "'%" . $search . "%' or e.paralelo LIKE " . "'%" . $search . "%' )" . (strlen($topic) != 0 ? "AND e.topic = '" . $topic . "'" : "") . " AND e.deleted = " . $deleted . " AND m.deleted = false")->getSingleScalarResult();
        if ($start == $recordsFilteredCount) {
            $start -= $start == 0 ? 0 : $lenght;
        }

        $recordsFilteredItems = $em->createQuery("SELECT e FROM ExaminrCoreBundle:Survey e INNER JOIN e.topic m WHERE (e.teacher LIKE " . "'%" . $search . "%' or e.paralelo LIKE " . "'%" . $search . "%' )" . (strlen($topic) != 0 ? "AND e.topic = '" . $topic . "'" : "") . " AND e.deleted = " . $deleted . " AND m.deleted = false ORDER BY " . ($sortColumn == 'module' ? "m.name" : ("e." . $sortColumn)) . " " . $sortOrder)
            ->setFirstResult($start)
            ->setMaxResults($lenght)
            ->getResult();

//        //CONCAT(d.estudiante_id, '') LIKE '%3%'

        return array(
            "recordsFiltered" => $recordsFilteredCount,
            "data" => $recordsFilteredItems
        );
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrCoreBundle:Survey e WHERE e.deleted = " . $deleted);

        return $qb->getSingleScalarResult();
    }

    public function allStudentHaventDone($login)
    {
        $em = $this->getEntityManager();

        return $em->createQuery("SELECT e FROM ExaminrCoreBundle:Survey e WHERE e.deleted = false AND e.active = true AND e NOT IN (SELECT e1 FROM ExaminrCoreBundle:SurveyAnswer ea INNER JOIN ea.survey e1 INNER JOIN ea.student s WHERE s.dni = '" . $login . "')")->getResult();
    }

    public function allActiveSurveys ()
    {
        $em = $this->getEntityManager();

        return $em->createQuery("SELECT partial e.{id,name} FROM ExaminrCoreBundle:Survey e WHERE e.active = true AND  e.deleted = false")->getResult();
    }

    public function allSurveys ()
    {
        $em = $this->getEntityManager();

        return $em->createQuery("SELECT e FROM ExaminrCoreBundle:Survey e")->getResult();
    }
}