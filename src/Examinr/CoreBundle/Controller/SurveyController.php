<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Exam;
use Examinr\CoreBundle\Entity\Module;
use Examinr\CoreBundle\Entity\Question;
use Examinr\CoreBundle\Entity\Survey;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\True;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class SurveyController extends Controller
{
    function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        return $this->render('ExaminrCoreBundle:Survey:index.html.twig', array(
            "topics" => $repository->allTopics()
        ));
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $URI = $request->server->get('REQUEST_URI');
        $deleted = strpos($URI, '/deleted/') ? 'true' : 'false';

        $filterData = $repository->filter($request, $deleted);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count($deleted),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');
        $modules = $repository->allTopics();
        $model = array(
            "topics" => $modules
        );

        if ($request->getMethod() == 'POST') {
            $errors = array();
            $error = false;
            $exam = new Survey();

            $exam->setName(trim($request->request->get('name')));
            if (strlen($exam->getName()) == 0) {
                $error = true;
                $errors[] = "The Survey name field can't be empty";
            }

            $exam->setTeacher(trim($request->request->get('teacher')));
            if (strlen($exam->getTeacher()) == 0) {
                $error = true;
                $errors[] = "The teacher field can't be empty";
            }

            $exam->setParalelo(trim($request->request->get('paralelo')));
            if (strlen($exam->getParalelo()) == 0) {
                $error = true;
                $errors[] = "The parallel field can't be empty";
            }

            $exam->setTopic($repository->findOneById($request->request->get('moduleFilter')));
            if (!$exam->getTopic()) {
                $errors[] = "The topic can't be null";
            }

            $exam->setActive($request->request->get('active') == 'on');
            $exam->setRandomizeQuestions($request->request->get('randomize') == 'on');

            if (sizeof($errors) == 0) {
                try {
                    $em->persist($exam);
                    $em->flush();

                    return $this->redirect($this->generateUrl('surveys'));
                } catch (\Exception $ex) {
                    $errors[] = $ex->getMessage();
                }
            }

            if (sizeof($errors) > 0) {
                $model['exam'] = $exam;
                $model['errors'] = $errors;
            }
        }

        return $this->render('ExaminrCoreBundle:Survey:new_edit.html.twig', $model);
    }

    function editAction(Request $request, $exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $mRrepository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');
        $eRepository = $em->getRepository('ExaminrCoreBundle:Survey');
        $topics = $mRrepository->allTopics();
        $model = array(
            "topics" => $topics
        );

        $exam = $eRepository->findOneById($exam_id);

        if (!$exam) {
            throw new NotFoundHttpException();
        }
        $model ['exam'] = $exam;

        if ($request->getMethod() == 'POST') {
            $errors = array();
            $error = false;

            $exam->setName(trim($request->request->get('name')));
            if (strlen($exam->getName()) == 0) {
                $error = true;
                $errors[] = "The Survey name field can't be empty";
            }

            $exam->setTeacher(trim($request->request->get('teacher')));
            if (strlen($exam->getTeacher()) == 0) {
                $error = true;
                $errors[] = "The teacher field can't be empty";
            }

            $exam->setParalelo(trim($request->request->get('paralelo')));
            if (strlen($exam->getParalelo()) == 0) {
                $error = true;
                $errors[] = "The parallel field can't be empty";
            }

            $exam->setTopic($mRrepository->findOneById($request->request->get('moduleFilter')));
            if (!$exam->getTopic()) {
                $errors[] = "The topic can't be null";
            }

            $exam->setActive($request->request->get('active') == 'on');
            $exam->setRandomizeQuestions($request->request->get('randomize') == 'on');

            if (sizeof($errors) == 0) {
                try {
                    $em->persist($exam);
                    $em->flush();

                    return $this->redirect($this->generateUrl('surveys'));
                } catch (\Exception $ex) {
                    $errors[] = $ex->getMessage();
                }
            }

            if (sizeof($errors) > 0) {
                $model['exam'] = $exam;
                $model['errors'] = $errors;
            }
        }

        return $this->render('ExaminrCoreBundle:Survey:new_edit.html.twig', $model);
    }

    function setActiveAction($exam_id)
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setActive(!$exam->getActive());

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deleteAction($exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setDeleted(true);

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deletedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        return $this->render('ExaminrCoreBundle:Survey:deleted.html.twig', array(
            "modules" => $repository->allModules()
        ));
    }

    function undeleteAction($exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setDeleted(false);

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    /**
     * Creates an xlsx file with the survey data and analysis
     *
     * @param Request $request
     */
    function renderResultAction($exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $SRepository = $em->getRepository('ExaminrCoreBundle:SurveyAnswer');
            $responses = $SRepository->findBySurvey($exam_id);

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            $phpExcelObject->getProperties()->setCreator("Examinr")
                ->setLastModifiedBy("Examinr")
                ->setTitle($exam->getName() . '-' . $exam->getTeacher() . '-' . $exam->getParalelo() . '-Results')
                ->setSubject("Resultados de Encuesta");
            $phpExcelObject->setActiveSheetIndex(0);

            $SQepository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');
            $questions = $SQepository->getFullObjectsByTopic($exam->getTopic()->getId());

            $phpExcelObject->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
            $phpExcelObject->getActiveSheet()->setCellValue('A1', "Fecha");
            foreach ($questions as $key => $question) {
                $phpExcelObject->getActiveSheet()->getColumnDimension($this->getExcelColumnByIndex($key + 1))->setAutoSize(true);
                $phpExcelObject->getActiveSheet()->setCellValue($this->getExcelColumnByIndex($key + 1) . 1, $question->getCleanText());
            }

            $key = 0;
            $SAQepository = $em->getRepository('ExaminrCoreBundle:SurveyAnswerQuestion');
            foreach ($responses as $response) {
                $resAnswers = $SAQepository->findByAnswer($response->getId());
                $phpExcelObject->getActiveSheet()->setCellValue('A' . ($key + 2), $response->date);

                $ckey = 0;
                foreach ($questions as $question) {
                    foreach ($resAnswers as $qa) {
                        if ($qa->question->getId() == $question->getId()){
                            $phpExcelObject->getActiveSheet()->setCellValue($this->getExcelColumnByIndex($ckey + 1) . ($key + 2), $qa->answer);
                            break;
                        }
                    }
                    $ckey ++;
                }
                $key ++;
            }

            $phpExcelObject->getActiveSheet()->setTitle('Respuestas');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);

            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $exam->getName() . '-' . $exam->getTeacher() . '-' . $exam->getParalelo() . '-Results' . '.xls'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }
        throw new NotFoundHttpException();
    }

    function validateQuestion($question)
    {
        $correctOptionsCount = 0;
        $options = array();
        $optionsCount = 0;
        $state = "text";
        $text = '';
        $tmp = "";

        for ($i = 0; $i < strlen($question); $i++) {
            $char = $question[$i];

            if ($state == 'text') {
                if ($char == '[') {
                    $state = 'open_option';
                } else {
                    $text .= $char;
                }
            } else if ($state == 'open_option') {
                if ($char == '[') {
                    $state = 'correct_option';
                } else if ($char == ']') {
                    return false;
                } else {
                    $state = 'option';
                    $tmp .= $char;
                }
            } else if ($state == 'option') {
                if ($char == ']') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'correct_option') {
                if ($char == ']') {
                    $state = 'close_correct_option';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'close_correct_option') {
                if ($char == ']') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $correctOptionsCount++;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $state = 'correct_option';
                    $tmp .= ']' . $char;
                }
            }
        }

        return ($optionsCount > 1 && $correctOptionsCount == 1 && $state == 'text');
    }

    function getExcelColumnByIndex($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
}
