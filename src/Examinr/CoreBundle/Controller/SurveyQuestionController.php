<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Module;
use Examinr\CoreBundle\Entity\Question;
use Examinr\CoreBundle\Entity\SurveyQuestion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SurveyQuestionController extends Controller
{
    function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        return $this->render('ExaminrCoreBundle:SurveyQuestion:index.html.twig', array(
            "topics" => $repository->allTopics()
        ));
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');

        $URI = $request->server->get('REQUEST_URI');
        $deleted = strpos($URI, '/deleted/') ? 'true' : 'false';

        $filterData = $repository->filter($request, $deleted);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count($deleted),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');
        $topics = $repository->allTopics();
        $model = array(
            "topics" => $topics
        );

        if ($request->getMethod() == 'POST') {

            $topic = $repository->findOneById($request->request->get('moduleFilter'));
            $question = new SurveyQuestion();

            $question->setTopic($topic);
            $question->setText($request->request->get('text'));

            $model['question'] = $question;

            if ($this->validateQuestion($question->getText())) {
                var_dump('all good');
                $em->persist($question);
                $em->flush();
                return $this->redirect($this->generateUrl('survey_questions'));
            } else if (strlen($question->getText()) == 0) {
                $model['errors'] = "The field text cannot be empty.";
            }
        }

        return $this->render('ExaminrCoreBundle:SurveyQuestion:new_edit.html.twig', $model);
    }

    function editAction(Request $request, $question_id)
    {
        $em = $this->getDoctrine()->getManager();
        $mRepository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');
        $qRepository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');
        $topics = $mRepository->allTopics();
        $model = array(
            "topics" => $topics
        );

        if ($request->getMethod() == 'GET') {
            $question = $qRepository->findOneById($question_id);

            if (!$question) {
                throw new NotFoundHttpException();
            }
            $model ['question'] = $question;
        } else if ($request->getMethod() == 'POST') {
            $topic = $mRepository->findOneById($request->request->get('moduleFilter'));
            $question = $qRepository->findOneById($question_id);

            $question->setTopic($topic);
            $question->setText($request->request->get('text'));

            $model['question'] = $question;

            if ($this->validateQuestion($question->getText())) {
                var_dump('all good');
                $em->persist($question);
                $em->flush();
                return $this->redirect($this->generateUrl('questions'));
            } else if (strlen($question->getText()) == 0) {
                $model['errors'] = "The field text cannot be empty.";
            }
        }

        return $this->render('ExaminrCoreBundle:SurveyQuestion:new_edit.html.twig', $model);
    }

    function deleteAction($question_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');

        $question = $repository->findOneById($question_id);

        if ($question) {
            $question->setDeleted(true);

            $em->persist($question);
            $em->flush();

            $data = array(
                'id' => $question->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deletedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        return $this->render('ExaminrCoreBundle:SurveyQuestion:deleted.html.twig', array(
            "modules" => $repository->allModules()
        ));
    }

    function undeleteAction($question_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');

        $question = $repository->findOneById($question_id);

        if ($question) {
            $question->setDeleted(false);

            $em->persist($question);
            $em->flush();

            $data = array(
                'id' => $question->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function validateQuestion($question)
    {
        $correctOptionsCount = 0;
        $options = array();
        $optionsCount = 0;
        $state = "text";
        $text = '';
        $tmp = "";

        for ($i = 0; $i < strlen($question); $i++) {
            $char = $question[$i];

            if ($state == 'text') {
                if ($char == '(') {
                    $state = 'open_option';
                } else {
                    $text .= $char;
                }
            } else if ($state == 'open_option') {
                if ($char == '(') {
                    $state = 'correct_option';
                } else if ($char == ']') {
                    return false;
                } else {
                    $state = 'option';
                    $tmp .= $char;
                }
            } else if ($state == 'option') {
                if ($char == ')') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'correct_option') {
                if ($char == ')') {
                    $state = 'close_correct_option';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'close_correct_option') {
                if ($char == ')') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $correctOptionsCount++;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $state = 'correct_option';
                    $tmp .= ')' . $char;
                }
            }
        }

        return ($optionsCount > 1 && $state != 'open_option' && $state != 'option' && $state != 'correct_option');
    }
}
