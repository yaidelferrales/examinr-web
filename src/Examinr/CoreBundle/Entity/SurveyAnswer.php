<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Examinr\CoreBundle\Repository\SurveyAnswerRepository;

/**
 * Module
 *
 * @ORM\Table(name="surveyanswer")
 * @ORM\Entity (repositoryClass="Examinr\CoreBundle\Repository\SurveyAnswerRepository")
 */
class SurveyAnswer implements JsonSerializable
{

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finished", type="boolean", nullable=false)
     */
    public $finished = false;

    /**
     * @ManyToOne(targetEntity="Examinr\SecurityBundle\Entity\Student")
     * @JoinColumn(name="student_id", referencedColumnName="id")
     **/
    public $student;

    /**
     * @ManyToOne(targetEntity="Examinr\CoreBundle\Entity\Survey")
     * @JoinColumn(name="survey_id", referencedColumnName="id")
     **/
    public $survey;

    /**
     * @OneToMany(targetEntity="Examinr\CoreBundle\Entity\SurveyAnswerQuestion", mappedBy="surveyanswer", orphanRemoval=true)
     **/
    public $questionAnswers;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;


    public function __construct()
    {
        $this->date = new \DateTime('NOW');
        $this->questionAnswers = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'date' => $this->date->format('d/m/Y h:i:s'),
            'survey' => $this->survey->getName(),
            'student' => $this->student->getDni(),'result' => $this->finished ? $this->getGrade() . "/" . $this->survey->getBase() : "0"
        );
    }

    public function jsonSerializeForServer()
    {
        return array(
            'survey' => $this->survey->getId(),
            'student' => $this->student->getDni()
        );
    }

    public function getId() {
        return $this->id;
    }
}
