<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Examinr\CoreBundle\Entity\Module;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use JsonSerializable;

/**
 * Question
 *
 * @ORM\Table(name="surveyquestion")
 * @ORM\Entity (repositoryClass="Examinr\CoreBundle\Repository\SurveyQuestionRepository")
 */
class SurveyQuestion implements JsonSerializable
{
    /**
     * @var \Examinr\CoreBundle\Entity\SurveyTopic
     *
     * @ORM\ManyToOne(targetEntity="Examinr\CoreBundle\Entity\SurveyTopic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     * })
     */
    private $topic;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=500, nullable=false)
     */
    private $text;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set topic
     *
     * @param integer $topic
     * @return SurveyQuestion
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return SurveyTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getCleanText()
    {
        return substr($this->text, 0, strpos($this->text, "("));
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Question
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            "id" => $this->id,
            "topic" => $this->topic->getName(),
            "text" => $this->text
        );
    }
}
