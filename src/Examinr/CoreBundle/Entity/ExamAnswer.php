<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;


/**
 * Module
 *
 * @ORM\Table(name="examanswer")
 * @ORM\Entity (repositoryClass="Examinr\CoreBundle\Repository\ExamAnswerRepository")
 */
class ExamAnswer implements JsonSerializable
{

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    public $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="correct", type="integer")
     */
    public $correct = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finished", type="boolean", nullable=false)
     */
    public $finished = false;

    /**
     * @ManyToOne(targetEntity="Examinr\SecurityBundle\Entity\Student")
     * @JoinColumn(name="student_id", referencedColumnName="id")
     **/
    public $student;

    /**
     * @ManyToOne(targetEntity="Examinr\CoreBundle\Entity\Exam")
     * @JoinColumn(name="exam_id", referencedColumnName="id")
     **/
    public $exam;

    /**
     * @OneToMany(targetEntity="Examinr\CoreBundle\Entity\ExamAnswerQuestion", mappedBy="examAnswer", orphanRemoval=true)
     **/
    public $questionAnswers;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;


    public function __construct()
    {
        $this->date = new \DateTime('NOW');
        $this->questionAnswers = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'date' => $this->date->format('d/m/Y h:i:s'),
            'exam' => $this->exam->getName(),
            'student' => $this->student->getDni(),
            'result' => $this->finished ? $this->getGrade() . "/" . $this->exam->getBase() : "0"
        );
    }

    public function jsonSerializeForServer()
    {
        return array(
            'exam' => $this->exam->getId(),
            'student' => $this->student->getDni(),
            'result' => $this->finished ? $this->getGrade() : 0
        );
    }

    public function getGrade () {
        $totalQuestions = sizeof($this->questionAnswers);
        $grade = $this->correct * $this->exam->getBase() / $totalQuestions;
        return round($grade, 0);
    }

    public function getId() {
        return $this->id;
    }
}
