<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;


/**
 * ExamAnswerQuestion
 *
 * @ORM\Table(name="examanswer_question")
 * @ORM\Entity (repositoryClass="Examinr\CoreBundle\Repository\ModuleRepository")
 */
class ExamAnswerQuestion
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="correct", type="boolean", nullable=false)
     */
    public $correct = false;

    /**
     * @ManyToOne(targetEntity="Question")
     * @JoinColumn(name="question_id", referencedColumnName="id")
     **/
    public $question;

    /**
     * @var \Examinr\CoreBundle\Entity\ExamAnswer
     *
     * @ORM\ManyToOne(targetEntity="Examinr\CoreBundle\Entity\ExamAnswer", inversedBy="questionAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examanswer_id", referencedColumnName="id")
     * })
     */
    public $examAnswer;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=false)
     */
    public $answer = "";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }
}
